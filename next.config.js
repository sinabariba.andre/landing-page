const withPlugins = require("next-compose-plugins");
const optimizedImages = require("next-optimized-images");

// next.config.js

module.exports = withPlugins([optimizedImages], {
  target: "serverless",
  images: {
    disableStaticImages: true,
  },
});
