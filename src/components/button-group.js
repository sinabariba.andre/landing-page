/** @jsx jsx */
/** @jsxRuntime classic */
import { jsx, Box, Container, Flex } from "theme-ui";
import { IoIosArrowDropleft, IoIosArrowDropleftCircle, IoIosArrowDropright, IoIosArrowDroprightCircle, IoIosArrowForward, IoIosArrowRoundBack, IoIosArrowRoundForward, IoIosReturnLeft } from "react-icons/io";

export default function ButtonGroup({ next, previous }) {
  return (
    <Flex sx={{ width: "100%" }}>
      <Container>
        <Box sx={styles.buttonGroup} className="button__group">
          <button onClick={previous} arial-label="Previous">
            <IoIosArrowDropleft />
          </button>
          <button onClick={next} arial-label="Next">
            <IoIosArrowDropright />
          </button>
        </Box>
      </Container>
    </Flex>
  );
}

const styles = {
  buttonGroup: {
    display: "flex",
    justifyContent: "center",
    mb: -4,
    button: {
      bg: "transparent",
      border: "0px solid",
      fontSize: 40,
      cursor: "pointer",
      px: "2px",
      color: "#BBC7D7",
      transition: "all 0.25s",
      "&:hover": {
        color: "text",
      },
      "&:focus": {
        outline: 0,
      },
    },
  },
};
